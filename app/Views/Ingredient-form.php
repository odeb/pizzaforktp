<?= $this->extend('page.php') ?>
<?= $this->section('body') ?>
<div class="card">
    <div class="card-header">
        <h1><?= $title ?></h1>
    </div>
    <div class="card-body">
        <?= ((session()->has('errors')) ? \Config\Services::validation()->listErrors() : '') ?>
        <form class="form-horizontal" action="<?= (isset($ingredient) ? '/ingredient/save/' . $ingredient->id : '/ingredient/save') ?>" method="post">
            <div class=form-group>
                <form-label for="text ">Ingredient : </form-label>
                <input type="text" name="text" id="text" value="<?= old('text', $ingredient->text ?? '', false) ?>" placeholder="Nom du nouvel ingrédient">
            </div>
            <button class="btn btn-primary" type="submit">
                <i class="fa fa-plus"> Valider</i>
            </button>
        </form>
    </div>
</div>
<?= $this->endSection() ?>