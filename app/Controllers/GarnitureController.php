<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\GarnitureModel;
use App\Entities\Garniture;
use App\Models\IngredientModel;
use App\Models\PizzaModel;

class GarnitureController extends BaseController {
    /** @var GarnitureModel $garnitureModel */
    protected $garnitureModel;
    protected $pizzaModel;

    public function __construct() {
        $this->helpers = ['form', 'url'];
        $this->garnitureModel = new GarnitureModel();
        $this->pizzaModel = new PizzaModel();
    }

    public function index($idPizza) {
    }

    public function create(int $idPizza) {
    }

    public function delete(int $id) {
    }

    public function save(int $id = null) {
    }

    public function edit(int $id) {
    }
}
